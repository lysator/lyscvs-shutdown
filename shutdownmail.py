#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

TOP = """Subject: LysCVS tas snart bort. Flytta din kod!
From: root@lysator.liu.se
Content-type: text/plain; encoding=utf-8

Lysator har länge tillhandahållit hosting av CVS- och
Subversion-repon genom LysCVS-projektet.  Lösningen har
underhållits av frivilliga, men sedan några år har det varit
väldigt ont om frivilliga.  Därför kommer servern som har hand om
CVS och Subversion att stängas ner inom kort.

På grund av säkerhetsproblem har vi redan nu tvingats att utan
förvarning låsa ner systemet så att det endast är nåbart från
Lysators nät.

Vi planerar att stänga av datorn någon gång efter 2014-09-30.

Detta påverkar dig, eftersom du är administratör för eller
medlem av ett eller flera projekt som listas nedan.

Varje projekt har nu fyra val:

 - Migrera projektet till Git.  Lysator har en Gitlab-installation
   på https://git.lysator.liu.se som gärna hostar ditt projekt.
   Detta är den rekommenderade lösningen.

 - Anmäl dig som frivillig att ta hand om LysCVS-systemet.  Du
   behöver kunna (eller lära dig) Webware, Cheetah-templates,
   Debian och Python.  För tillfället körs tjänsterna på en Debian
   4-maskin -- de måste flyttas till en dator som kör ett modernt
   system, t ex Debian 7.  Ingen kommer att tacka dig, och det
   kommer inte att se bra ut på ditt CV att du år 2014 började
   jobba med CVS eller Subversion.  Denna lösning rekommenderas
   inte.

 - Hitta någon annan site som vill hosta ditt CVS- eller
   Subversion-projekt.

 - Flytta ditt CVS-projekt till /lysator/cvsroot.  Det finns inga
   planer på att ta bort den arean.  Men den är inte heller nåbar
   utanför Lysators nät, så om externa personer är inblandade är
   detta ingen bra lösning.  Men kortsiktigt är det kanske enklare
   än att migrera till Git.

Om du väljer att migrera till Git kan du hitta tips om hur du kan
göra på
<https://datorhandbok.lysator.liu.se/index.php/Gitmigrering>.

De projekt som berör dig listas här, först de som du är administratör
för, och sedan de som du bara är medlem i.  För varje projekt listas
alla andra medlemmar i projektet.

"""

BOTTOM = """Med vänlig hälsning,

    root@lysator.liu.se
"""

class Recipient(object):
    def __init__(self, email):
        self.__email = email
        self.__admin_projects = []
        self.__nonadmin_projects = []

    def add_project(self, project, admin):
        if admin:
            self.__admin_projects.append(project)
        else:
            self.__nonadmin_projects.append(project)

    def email(self):
        return self.__email

    def spool_mail(self, outdir):
        s = "To: <" + self.__email + ">\n" + TOP
        s += self.__spool_projects("Projekt som du är administratör för:",
                                   self.__admin_projects)
        s += self.__spool_projects("Projekt som du är medlem i (men ej admin för):",
                                   self.__nonadmin_projects)
        s += BOTTOM
        fp = open(os.path.join(outdir, self.email()), "w")
        fp.write(s)
        fp.close()

    def __spool_projects(self, header, projs):
        s = ""
        if len(projs) == 0:
            return s
        s += header + "\n"
        s += "=" * len(header) + "\n\n"
        for proj in sorted(projs):
            s += "Projekt: " + proj.name() + " (" + proj.vcs() + ")\n"
            for [other, other_admin] in proj.members():
                if other.email() != self.__email:
                    s += "  " + other.email()
                    if other_admin:
                        s += " (admin)"
                    s += "\n"
            s += "\n"
        return s

class Project(object):
    def __init__(self, name, vcs):
        self.__name = name
        self.__vcs = vcs
        self.__members = []

    def vcs(self):
        return self.__vcs

    def name(self):
        return self.__name

    def members(self):
        return self.__members

    def add_member(self, r, admin):
        self.__members.append((r, admin))

class Collection(object):
    def __init__(self):
        self.__recipients = {}
        self.__projects = {}

    def parse(self, filename):
        for line in open(filename).readlines():
            [email, vcs, proj, admin] = line.split("\t")
            admin = (admin == "A\n")

            if email not in self.__recipients:
                self.__recipients[email] = Recipient(email)

            k = vcs + "_" + proj
            if k not in self.__projects:
                self.__projects[k] = Project(proj, vcs)

            p = self.__projects[k]
            r = self.__recipients[email]

            r.add_project(p, admin)
            p.add_member(r, admin)

    def spool_mail(self, outdir):
        for r in self.__recipients.values():
            r.spool_mail(outdir)

def main():
    c = Collection()
    c.parse("groups_and_users.out")
    c.spool_mail("shutdown-spool")

if __name__ == '__main__':
    main()
